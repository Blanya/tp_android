package com.example.exercices_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class ContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        receiveContact();
    }

    private void receiveContact(){
        Intent intent = getIntent();
        String[] contact = intent.getStringArrayExtra("contact");
        TextView phone = findViewById(R.id.contactPhone);
        TextView name = findViewById(R.id.contactName);
        phone.setText(contact[0]);
        String allName = contact[1] + " " + contact[2];
        name.setText("Bonjour " + allName);
    }
}