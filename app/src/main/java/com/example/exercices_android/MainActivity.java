package com.example.exercices_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import java.sql.Array;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        submitContact();
    }

    private void submitContact(){
        Button b = findViewById(R.id.buttonSubmit);
        EditText phone = findViewById(R.id.phone);
        EditText nom = findViewById(R.id.nom);
        EditText prenom = findViewById(R.id.prenom);
        b.setOnClickListener(e -> {
            Intent intent = new Intent(this, ContactActivity.class);
            intent.putExtra("contact", new String[]{phone.getText().toString(), nom.getText().toString(), prenom.getText().toString()});
            startActivity(intent);
        });
    }
}