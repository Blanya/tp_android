package com.example.exercices_android;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;

import com.example.exercices_android.databinding.FragmentFirstBinding;
import com.example.exercices_android.model.Contact;
import com.example.exercices_android.service.ContactService;

public class FirstFragment extends Fragment {

    private FragmentFirstBinding binding;
    private ContactService contactService;
    private Contact contact;

    public FirstFragment(){
        contactService = new ContactService();
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        FirstFragmentArgs args = FirstFragmentArgs.fromBundle(getArguments());
        System.out.println(args.getIdContact());
        if(args.getIdContact() != -1){
            contact = contactService.getContactByIndex(args.getIdContact());
        }
        binding = FragmentFirstBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(contact != null){
            binding.prenom.setText(contact.getFirstName());
            binding.nom.setText(contact.getLastName());
            binding.phone.setText(contact.getPhone());
        }

        binding.buttonSubmit.setOnClickListener(e -> {
            //Afficher un contact ds second fragment
//            EditText phone = binding.phone;
//            EditText nom = binding.nom;
//            EditText prenom = binding.prenom;
//
//            String[] contacts = new String[]{prenom.getText().toString(), nom.getText().toString(), phone.getText().toString()};
//
//            NavDirections action = FirstFragmentDirections.actionFirstFragmentToSecondFragment(contacts);
//            NavHostFragment.findNavController(FirstFragment.this).navigate(action);

            EditText phone = binding.phone;
            EditText nom = binding.nom;
            EditText prenom = binding.prenom;

            if(contact == null){
                //Afficher une list view
                String[] contacts = new String[]{prenom.getText().toString(), nom.getText().toString(), phone.getText().toString()};

                binding.phone.setText(null);
                binding.nom.setText(null);
                binding.prenom.setText(null);

                NavDirections action = FirstFragmentDirections.actionFirstFragmentToListContactFragment(contacts);
                NavHostFragment.findNavController(FirstFragment.this).navigate(action);
            }else {
                contact.setFirstName(prenom.getText().toString());
                contact.setLastName(nom.getText().toString());
                contact.setPhone(phone.getText().toString());

                NavDirections action = FirstFragmentDirections.actionFirstFragmentToListContactFragment(null);
                NavHostFragment.findNavController(FirstFragment.this).navigate(action);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}