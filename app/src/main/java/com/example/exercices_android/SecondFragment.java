package com.example.exercices_android;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.exercices_android.databinding.FragmentSecondBinding;
import com.example.exercices_android.model.Contact;

public class SecondFragment extends Fragment {

    private FragmentSecondBinding binding;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentSecondBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.buttonSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        SecondFragmentArgs args = SecondFragmentArgs.fromBundle(getArguments());
        binding.contactName.setText("Bonjour " + args.getContact()[0] + " " + args.getContact()[1]);
        binding.contactPhone.setText(args.getContact()[2]);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}