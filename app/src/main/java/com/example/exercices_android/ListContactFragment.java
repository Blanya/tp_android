package com.example.exercices_android;

import static com.example.exercices_android.ContactWithFragment.contactList;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.exercices_android.databinding.FragmentListContactBinding;
import com.example.exercices_android.model.Contact;
import com.example.exercices_android.service.ContactService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ListContactFragment extends Fragment {

    FragmentListContactBinding binding;
    ContactService contactService;

    Contact contact;

    public ListContactFragment() {
        // Required empty public constructor
        contactService = new ContactService();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ListContactFragmentArgs args = ListContactFragmentArgs.fromBundle(getArguments());
            if(args.getContact() != null) {
                contact = new Contact(args.getContact()[0], args.getContact()[1], args.getContact()[2]);
                contactService.add(contact);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentListContactBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        binding.buttonList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ListContactFragment.this)
                        .navigate(R.id.action_ListContactFragment_to_FirstFragment);
            }
        });

//        SecondFragmentArgs args = SecondFragmentArgs.fromBundle(getArguments());
//        String contact = args.getContact()[0] + " " + args.getContact()[1] + " " + args.getContact()[2];
//        contactList.add(contact);

//        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(getContext(), androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, contactList);

        ArrayAdapter<Contact> stringArrayAdapter = new ArrayAdapter<Contact>(getContext(), androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, contactService.getContacts());

        binding.listViewContacts.setAdapter(stringArrayAdapter);
//        binding.listViewContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Contact contactToSend = (Contact) binding.listViewContacts.getItemAtPosition(position);
//
//                String[] contacts = new String[]{contactToSend.getFirstName(), contactToSend.getLastName(), contactToSend.getPhone()};
//
//                NavDirections action =  ListContactFragmentDirections.actionListContactFragmentToSecondFragment(contacts);
//                NavHostFragment.findNavController(ListContactFragment.this).navigate(action);
//            }
//        });

        binding.listViewContacts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("selected");
                binding.buttonUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NavDirections action =  ListContactFragmentDirections.actionListContactFragmentToFirstFragment().setIdContact(position);
                        NavHostFragment.findNavController(ListContactFragment.this).navigate(action);
                    }
                });
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//                binding.buttonUpdate.setEnabled(false);
            }
        });


    }
}